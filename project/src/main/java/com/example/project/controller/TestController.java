package com.example.project.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class TestController {

    @GetMapping("/")
    public String home(){
        String res = "success";
        log.info("TestController.home res:{}",res);
        return res;
    }

    @GetMapping("/test")
    public String test(){
        String res = "TestController.test";
        log.info("TestController.test res:{}",res);
        return res;
    }
}
